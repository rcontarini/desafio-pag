package br.com.rcontarini.movies.data.network.model

import java.io.Serializable

class BaseGenreResponse(val genres : List<GenreResponse>) : Serializable