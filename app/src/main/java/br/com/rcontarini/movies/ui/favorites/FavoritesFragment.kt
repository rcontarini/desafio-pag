package br.com.rcontarini.movies.ui.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import br.com.rcontarini.movies.R
import br.com.rcontarini.movies.data.network.model.MoviesResponse
import br.com.rcontarini.movies.di.component.DaggerFavoritePresenterComponent
import br.com.rcontarini.movies.di.module.FavoritePresenterModule
import br.com.rcontarini.movies.ui.genericDialog.GenericDialog
import br.com.rcontarini.movies.ui.genericDialog.GenericDialogImageType
import br.com.rcontarini.movies.ui.moviesDetail.createMovieDetailIntent
import br.com.rcontarini.movies.utils.extensions.setup
import br.com.rcontarini.movies.utils.extensions.startActivitySlideTransition
import kotlinx.android.synthetic.main.fragment_favorite.*
import javax.inject.Inject

class FavoritesFragment : Fragment(), FavoriteContract.View {

    lateinit var gridLayoutManager: GridLayoutManager
    @Inject lateinit var mPresenter : FavoritePresenter

    private val mAdapter by lazy {
        val adapter = FavoriteAdapter(activity!!.applicationContext, object : FavoriteAdapter.OnItemClickListener{
            override fun onItemClicked(item: MoviesResponse) {
                mPresenter.onClickItem(item)
            }
        })
        gridLayoutManager  = GridLayoutManager(activity!!.applicationContext, 3)
        rvFavorite.setup(adapter, gridLayoutManager)
        adapter
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        mPresenter.attach(this)
        mPresenter.getMovies()
    }

    override fun displayError(msg: String?) {
        GenericDialog(context!!, imageType = GenericDialogImageType.FACE_SAD, title = getString(R.string.dialog_error), htmlSubtitle = msg,
            negativeButtonTitle = null, positiveButtonTitle = getString(R.string.positive_button), onDialogClickListener = object : GenericDialog.OnDialogClickListener{
                override fun onDialogPositiveClicked() {
                }

                override fun onDialogNegativeClicked() {
                }

            }, isCancelable = false).show()
    }


    override fun displayMovies(movies: List<MoviesResponse>) {
        mAdapter.setMovies(movies)
        mAdapter.notifyDataSetChanged()
        showList()
    }

    private fun showList() {
        rvFavorite.visibility = View.VISIBLE
    }

    override fun loadMoviesDetail(item: MoviesResponse) {
        activity!!.startActivitySlideTransition(activity!!.createMovieDetailIntent(item))
    }

    override fun showLoading(loading: Boolean) {
        if (loading) {
            pbFavorite.visibility = View.VISIBLE
        } else {
            pbFavorite.visibility = View.GONE
        }
    }

    private fun injectDependency() {
        val build = DaggerFavoritePresenterComponent.builder()
            .favoritePresenterModule(FavoritePresenterModule()).build()
        build.inject(this)
    }

}