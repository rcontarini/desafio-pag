package br.com.rcontarini.movies.ui.favorites

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.rcontarini.movies.R
import br.com.rcontarini.movies.data.network.model.MoviesResponse
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie_favorite.view.*


class FavoriteAdapter(private val context: Context, private val onItemClickListener: OnItemClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mList: ArrayList<MoviesResponse> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_movie_favorite, parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is ItemViewHolder){
            holder.bind(mList[position], onItemClickListener)
        }
    }

    fun setMovies(listMoviesResponse: List<MoviesResponse>){
        mList.addAll(listMoviesResponse)
    }

    interface OnItemClickListener{
        fun onItemClicked( item : MoviesResponse )
    }

    inner class ItemViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        fun bind(item : MoviesResponse, onItemClickListerner: OnItemClickListener){

            itemView.apply {
                setOnClickListener {
                    onItemClickListerner.onItemClicked( item )
                }

                Picasso.get().load(item.getMoviePath()).into(moviePosterFavorite)

                if(item.isFavorite){
                    movieFavorite.visibility = View.VISIBLE
                }

            }
        }
    }
}