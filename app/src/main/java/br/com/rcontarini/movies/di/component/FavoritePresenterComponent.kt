package br.com.rcontarini.movies.di.component

import br.com.rcontarini.movies.di.module.FavoritePresenterModule
import br.com.rcontarini.movies.ui.favorites.FavoritesFragment
import dagger.Component

@Component(modules = [FavoritePresenterModule::class])
interface FavoritePresenterComponent {

    fun inject(favoritesFragment: FavoritesFragment)
}