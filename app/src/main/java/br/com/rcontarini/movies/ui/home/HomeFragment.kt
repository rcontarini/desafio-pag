package br.com.rcontarini.movies.ui.home

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.rcontarini.movies.R
import br.com.rcontarini.movies.data.network.model.MoviesResponse
import br.com.rcontarini.movies.di.component.DaggerHomeFragmentComponent
import br.com.rcontarini.movies.di.module.HomeFragmentModule
import br.com.rcontarini.movies.ui.genericDialog.GenericDialog
import br.com.rcontarini.movies.ui.genericDialog.GenericDialogImageType
import br.com.rcontarini.movies.ui.moviesDetail.createMovieDetailIntent
import br.com.rcontarini.movies.utils.extensions.setup
import br.com.rcontarini.movies.utils.extensions.startActivitySlideTransition
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : Fragment(), HomeContract.View {

    @Inject lateinit var mPresenter : HomePresenter
    private var pageCount : Int = 1
    lateinit var layoutManager : LinearLayoutManager

    private val mAdapter by lazy {
        val adapter = HomeAdapter(activity!!.applicationContext, object : HomeAdapter.OnItemClickListener{
            override fun onItemClicked(item: MoviesResponse) {
                mPresenter.onClickItem(item)
            }
        })
        layoutManager = LinearLayoutManager(context)
        rvHome.setup(adapter, layoutManager)
        adapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPresenter.attach(this)
        initView()
        setListeners()
    }

    private fun setListeners(){
        rvHome.addOnScrollListener( object  : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val itemCount = layoutManager.itemCount
                val childCount = layoutManager.childCount
                val visibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition()

                if(progressBar.visibility == View.GONE){
                    if(visibleItemPosition + childCount >= itemCount - 1){
                        pageCount++
                        mPresenter.getMovies(pageCount)
                    }
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })
    }

    private fun initView() {
        mPresenter.getMovies(pageCount)
    }

    override fun displayError(msg: String?) {
        GenericDialog(context!!, imageType = GenericDialogImageType.FACE_SAD, title = getString(R.string.dialog_error), htmlSubtitle = msg,
            negativeButtonTitle = null, positiveButtonTitle = getString(R.string.positive_button), onDialogClickListener = object : GenericDialog.OnDialogClickListener{
                override fun onDialogPositiveClicked() {
                }

                override fun onDialogNegativeClicked() {
                }

            }, isCancelable = false).show()
    }

    private fun showList(){
        rvHome.visibility = View.VISIBLE
    }

    override fun displayMovies(movies: List<MoviesResponse>) {
        mAdapter.setMovies(movies)
        mAdapter.notifyDataSetChanged()
        showList()
    }

    override fun loadMoviesDetail(item: MoviesResponse) {
        activity!!.startActivitySlideTransition(activity!!.createMovieDetailIntent(item))
    }

    override fun showLoading(loading: Boolean) {
        if (loading) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }

    private fun injectDependency() {
        val build =
            DaggerHomeFragmentComponent.builder().homeFragmentModule(HomeFragmentModule()).build()
        build.inject(this)
    }

}