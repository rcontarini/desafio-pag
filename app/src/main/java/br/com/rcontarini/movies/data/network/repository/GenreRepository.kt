package br.com.rcontarini.movies.data.network.repository

import br.com.rcontarini.movies.data.network.datasource.GenreRemoteDataSource


object GenreRepository {

    fun getGenre() = GenreRemoteDataSource.getGenre()

}