package br.com.rcontarini.movies.ui.moviesDetail

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.rcontarini.movies.R
import br.com.rcontarini.movies.data.network.model.MoviesResponse
import br.com.rcontarini.movies.di.component.DaggerMovieDetailComponent
import br.com.rcontarini.movies.di.module.MovieDetailModule
import br.com.rcontarini.movies.ui.createMainActivityIntent
import br.com.rcontarini.movies.ui.genericDialog.GenericDialog
import br.com.rcontarini.movies.ui.genericDialog.GenericDialogImageType
import br.com.rcontarini.movies.utils.AppConstants
import br.com.rcontarini.movies.utils.extensions.startActivitySlideTransition
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_movie_detail.*
import kotlinx.android.synthetic.main.content_movie_detail.*
import org.jetbrains.anko.intentFor
import javax.inject.Inject

class MovieDetailActivity : AppCompatActivity(), MovieDetailContract.View {

    @Inject lateinit var mPresenter : MovieDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)
        setListeners()
        injectDependency()
        setup()
    }

    private fun setup() {
        mPresenter.attach(this)
        mPresenter.loadMovieDetail(intent.getSerializableExtra(AppConstants.MOVIES_DETAIL) as MoviesResponse)
    }


    override fun displayError(msg: String?) {
        GenericDialog(this, imageType = GenericDialogImageType.FACE_SAD, title = getString(R.string.dialog_error), htmlSubtitle = msg,
            negativeButtonTitle = null, positiveButtonTitle = getString(R.string.positive_button), onDialogClickListener = object : GenericDialog.OnDialogClickListener{
                override fun onDialogPositiveClicked() {
                }

                override fun onDialogNegativeClicked() {
                }

            }, isCancelable = false).show()
    }

    override fun showMoviesDetail(item: MoviesResponse) {
        Picasso.get().load(item.getMovieBanner()).into(bannerImage)
        movieOverview.text = item.getOverviewTrated()
        movieDateDetail.text = item.getDateFormat()
        toolbarDetail.title = item.title
        setSupportActionBar(toolbarDetail)
    }

    private fun setListeners(){
        fab.setOnClickListener { view ->
           mPresenter.onFavoriteClicked()
        }
    }

    override fun favoriteAdd(isFavorite : Boolean) {
        if(isFavorite){
            fab.setImageResource(R.drawable.ic_star_yellow_24dp)
        } else {
            fab.setImageResource(R.drawable.ic_star_white)
        }

    }

    override fun displaySuccess() {
        GenericDialog(this, imageType = GenericDialogImageType.FACE_SMILE_EYE_OUTLINE, title = getString(R.string.dialog_succes), htmlSubtitle = getString(R.string.favorite_success),
            negativeButtonTitle = null, positiveButtonTitle = getString(R.string.positive_button), onDialogClickListener = object : GenericDialog.OnDialogClickListener{
                override fun onDialogPositiveClicked() {
                }

                override fun onDialogNegativeClicked() {
                }

            }, isCancelable = false).show()

    }

    override fun questionRemove() {
        GenericDialog(this, imageType = GenericDialogImageType.QUESTION, title = getString(R.string.dialog_attention), htmlSubtitle = getString(R.string.favorite_question),
            negativeButtonTitle = getString(R.string.negative_button), positiveButtonTitle = getString(R.string.negative_question_button), onDialogClickListener = object : GenericDialog.OnDialogClickListener{
                override fun onDialogPositiveClicked() {
                    mPresenter.removeFavorite()
                }

                override fun onDialogNegativeClicked() {
                }

            }, isCancelable = false).show()
    }

    override fun displayRemoveSuccess() {
        GenericDialog(this, imageType = GenericDialogImageType.FACE_SMILE_EYE_OUTLINE, title = getString(R.string.dialog_succes), htmlSubtitle = getString(R.string.favorite_remove_success),
            negativeButtonTitle = null, positiveButtonTitle = getString(R.string.positive_button), onDialogClickListener = object : GenericDialog.OnDialogClickListener{
                override fun onDialogPositiveClicked() {
                    initActivityMain()
                }

                override fun onDialogNegativeClicked() {
                }

            }, isCancelable = false).show()
    }

    private fun initActivityMain(){
        startActivitySlideTransition(createMainActivityIntent())
    }

    private fun injectDependency() {
        val build =
            DaggerMovieDetailComponent.builder().movieDetailModule(MovieDetailModule()).build()
        build.inject(this)
    }
}

fun Context.createMovieDetailIntent(item : MoviesResponse) = intentFor<MovieDetailActivity>(
    AppConstants.MOVIES_DETAIL to item
)