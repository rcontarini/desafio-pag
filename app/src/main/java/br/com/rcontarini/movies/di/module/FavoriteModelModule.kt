package br.com.rcontarini.movies.di.module

import br.com.rcontarini.movies.ui.favorites.FavoriteModel
import dagger.Module
import dagger.Provides

@Module
class FavoriteModelModule {

    @Provides
    fun provideFavoriteModel() : FavoriteModel{
        return FavoriteModel()
    }
}