package br.com.rcontarini.movies.ui.moviesDetail

import br.com.rcontarini.movies.data.db.MovieDao
import br.com.rcontarini.movies.data.network.model.MoviesResponse
import br.com.rcontarini.movies.ui.base.BaseApp
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


class MovieDetailModel : MovieDetailContract.Model {


    lateinit var presenter: MovieDetailContract.Presenter
    var mMovieDao : MovieDao? = null

    init {
        mMovieDao = BaseApp.database?.movieDao()
    }


    override fun onFavoriteSave(item: MoviesResponse) {
        Completable.fromAction {
            mMovieDao?.insertMovieFavorite(item)
        }.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io()).subscribe( object : CompletableObserver{
                override fun onComplete() {
                    presenter.success()
                }
                override fun onSubscribe(d: Disposable) {
                }
                override fun onError(e: Throwable) {
                    presenter.error(e.message!!)
                }
            })
    }

    override fun onRemoveFavorite(item: MoviesResponse) {
        Completable.fromAction { mMovieDao?.delete(item) }.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io()).subscribe( object : CompletableObserver{
                override fun onComplete() {
                    presenter.successRemove()
                }
                override fun onSubscribe(d: Disposable) {
                }
                override fun onError(e: Throwable) {
                    presenter.error(e.message!!)
                }
            })
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
    }

    override fun attach(presenter: MovieDetailContract.Presenter) {
        this.presenter = presenter
    }

}