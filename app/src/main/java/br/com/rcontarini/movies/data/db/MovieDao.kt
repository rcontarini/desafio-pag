package br.com.rcontarini.movies.data.db

import androidx.room.*
import br.com.rcontarini.movies.data.network.model.MoviesResponse
import io.reactivex.Single


@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovieFavorite(movie: MoviesResponse)

    @Query("SELECT * FROM movie") fun getAllMovies(): Single<List<MoviesResponse>>

    @Delete fun delete(movie: MoviesResponse)

}