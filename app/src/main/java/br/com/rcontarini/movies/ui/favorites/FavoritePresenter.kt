package br.com.rcontarini.movies.ui.favorites

import br.com.rcontarini.movies.data.network.model.MoviesResponse
import br.com.rcontarini.movies.di.component.DaggerFavoriteModelComponent
import br.com.rcontarini.movies.di.module.FavoriteModelModule
import javax.inject.Inject

class FavoritePresenter : FavoriteContract.Presenter {

    private lateinit var view: FavoriteContract.View
    @Inject lateinit var mModel : FavoriteModel

    init {
        injectDependecy()
        mModel.attach(this)
    }

    override fun getMovies() {
        view.showLoading(true)
        mModel.loadMovies()
    }

    override fun onClickItem(item: MoviesResponse) {
        view.loadMoviesDetail(item)
    }

    override fun setMovies(movies: List<MoviesResponse>) {
        view.showLoading(false)
        view.displayMovies(movies)
    }

    override fun setError(error: Throwable) {
        view.showLoading(false)
        view.displayError(error.message)
    }

    override fun attach(view: FavoriteContract.View) {
        this.view = view
    }


    private fun injectDependecy() {
        val build =
            DaggerFavoriteModelComponent.builder().favoriteModelModule(FavoriteModelModule())
                .build()
        build.inject(this)
    }

}