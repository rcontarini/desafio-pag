package br.com.rcontarini.movies.utils

object NetworkConstants {

    private const val BASE_URL = "https://api.themoviedb.org/3/"
    const val MOVIES_API = BASE_URL + "movie/"
    const val GENRE_API = BASE_URL + "genre/movie/"
    const val API_KEY = "1f54bd990f1cdfb230adb312546d765d"
    const val LANGUAGE = "pt-BR"

    const val BASE_URL_MOVIE = "https://image.tmdb.org/t/p/w185/"
    const val CODE_UNKNOWN = 500
}