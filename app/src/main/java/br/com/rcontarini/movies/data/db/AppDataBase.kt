package br.com.rcontarini.movies.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import br.com.rcontarini.movies.data.network.model.GenreResponse
import br.com.rcontarini.movies.data.network.model.MoviesResponse

@Database(version = 5, entities = arrayOf(MoviesResponse::class, GenreResponse::class))
abstract class AppDataBase : RoomDatabase(){

    abstract fun movieDao() : MovieDao

}