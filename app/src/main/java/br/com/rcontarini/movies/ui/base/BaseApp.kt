package br.com.rcontarini.movies.ui.base

import android.app.Application
import androidx.room.Room
import br.com.rcontarini.movies.data.db.AppDataBase
import br.com.rcontarini.movies.di.component.ApplicationComponent
import br.com.rcontarini.movies.di.component.DaggerApplicationComponent
import br.com.rcontarini.movies.di.module.ApplicationModule

class BaseApp : Application() {

    lateinit var component : ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        database = Room.databaseBuilder(this, AppDataBase::class.java, "movie-db").build()

        instance = this

        initDagger()


    }

    private fun initDagger() {
        component = DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()
        component.inject(this)
    }

    fun getApplicationComponent() : ApplicationComponent {
        return component
    }

    companion object {
        lateinit var instance: BaseApp private set
        var database: AppDataBase? = null
    }

}