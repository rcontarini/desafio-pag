package br.com.rcontarini.movies.di.module

import br.com.rcontarini.movies.ui.home.HomeModel
import dagger.Module
import dagger.Provides

@Module
class HomePresenterModule {

    @Provides
    fun provideHomeModel() : HomeModel{
        return HomeModel()
    }
}