package br.com.rcontarini.movies.ui.home

import br.com.rcontarini.movies.data.network.repository.MoviesRepository
import br.com.rcontarini.movies.utils.extensions.singleSubscribe
import io.reactivex.disposables.CompositeDisposable

class HomeModel : HomeContract.Model {

    private var mDisposable = CompositeDisposable()
    private lateinit var presenter: HomeContract.Presenter

    override fun loadMovies(page : Int) {
        mDisposable.add(
            MoviesRepository.getMovies(page).singleSubscribe(
                onSuccess = {
                    presenter.setMovies(it.results)
                },

                onError = {
                    presenter.setError(it)
                }
            ))
    }

    override fun attach(presenter: HomeContract.Presenter) {
        this.presenter = presenter
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
        mDisposable.clear()
    }
}