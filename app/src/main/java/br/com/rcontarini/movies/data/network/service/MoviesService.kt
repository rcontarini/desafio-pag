package br.com.rcontarini.movies.data.network.service

import br.com.rcontarini.movies.data.network.model.BaseGenreResponse
import br.com.rcontarini.movies.data.network.model.BaseMoviesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesService {

    @GET("upcoming?")
    fun getMovies(@Query("api_key") api_key : String,
                  @Query("language") language : String,
                  @Query("page") page : Int): Single<BaseMoviesResponse>

    @GET(value = "list?")
    fun getGenre(@Query("api_key") api_key : String,
                 @Query("language") languege : String) : Single<BaseGenreResponse>
}