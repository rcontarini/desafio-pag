package br.com.rcontarini.movies.ui.home

import br.com.rcontarini.movies.data.network.model.MoviesResponse
import br.com.rcontarini.movies.di.component.DaggerHomeModelComponent
import br.com.rcontarini.movies.di.module.HomePresenterModule
import javax.inject.Inject

class HomePresenter : HomeContract.Presenter {

    private lateinit var view: HomeContract.View

    @Inject lateinit var mModel : HomeModel

    init {
        injectDependecy()
        mModel.attach(this)
    }

    override fun getMovies(page : Int) {
        view.showLoading(true)
        mModel.loadMovies(page)
    }

    override fun onClickItem(item: MoviesResponse) {
        view.loadMoviesDetail(item)
    }

    override fun setMovies(movies: List<MoviesResponse>) {
        view.showLoading(false)
        view.displayMovies(movies)
    }

    override fun setError(error: Throwable) {
        view.showLoading(false)
        view.displayError(error.message)
    }


    override fun attach(view: HomeContract.View) {
        this.view = view
    }

    private fun injectDependecy(){
        val build =
            DaggerHomeModelComponent.builder().homePresenterModule(HomePresenterModule()).build()
        build.inject(this)
    }
}