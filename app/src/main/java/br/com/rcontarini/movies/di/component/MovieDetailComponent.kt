package br.com.rcontarini.movies.di.component

import br.com.rcontarini.movies.di.module.MovieDetailModule
import br.com.rcontarini.movies.ui.moviesDetail.MovieDetailActivity
import dagger.Component

@Component(modules = arrayOf(MovieDetailModule::class))
interface MovieDetailComponent {

    fun inject(movieDetailActivity: MovieDetailActivity)
}