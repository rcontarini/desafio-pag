package br.com.rcontarini.movies.di.module

import br.com.rcontarini.movies.ui.home.HomePresenter
import dagger.Module
import dagger.Provides

@Module
class HomeFragmentModule {

    @Provides
    fun provideHomePresenter() : HomePresenter{
        return HomePresenter()
    }


}