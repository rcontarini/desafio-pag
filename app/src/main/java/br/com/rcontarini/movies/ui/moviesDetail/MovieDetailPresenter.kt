package br.com.rcontarini.movies.ui.moviesDetail

import br.com.rcontarini.movies.data.network.model.MoviesResponse
import br.com.rcontarini.movies.di.component.DaggerMovieDetailModelComponent
import br.com.rcontarini.movies.di.module.MoviePresenterModule
import javax.inject.Inject

class MovieDetailPresenter : MovieDetailContract.Presenter {

    private lateinit var view: MovieDetailContract.View
    private lateinit var mMoviesResponse: MoviesResponse
    @Inject lateinit var mMovieDetailModel : MovieDetailModel

    init {
        injectDependecy()
        mMovieDetailModel.attach(this)
    }

    override fun onFavoriteClicked() {
        if(!mMoviesResponse.isFavorite){
            mMoviesResponse.isFavorite = true
            view.favoriteAdd(mMoviesResponse.isFavorite)
            mMovieDetailModel.onFavoriteSave(mMoviesResponse)
        } else {
            view.questionRemove()
        }
    }

    override fun removeFavorite() {
        mMovieDetailModel.onRemoveFavorite(mMoviesResponse)
    }

    override fun loadMovieDetail(item: MoviesResponse) {
        mMoviesResponse = item
        view.showMoviesDetail(mMoviesResponse)
        view.favoriteAdd(mMoviesResponse.isFavorite)
    }

    override fun attach(view: MovieDetailContract.View) {
        this.view = view
    }

    private fun injectDependecy() {
        val build =
            DaggerMovieDetailModelComponent.builder().moviePresenterModule(MoviePresenterModule())
                .build()
        build.inject(this)
    }

    override fun success() {
        view.displaySuccess()
    }

    override fun successRemove() {
        mMoviesResponse.isFavorite = false
        view.favoriteAdd(false)
        view.displayRemoveSuccess()
    }

    override fun error(msg: String) {
        view.displayError(msg)
    }

}