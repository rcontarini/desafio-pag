package br.com.rcontarini.movies.di.component

import br.com.rcontarini.movies.di.module.FavoriteModelModule
import br.com.rcontarini.movies.ui.favorites.FavoritePresenter
import dagger.Component

@Component(modules = [FavoriteModelModule::class])
interface FavoriteModelComponent {

    fun inject(favoritePresenter: FavoritePresenter)
}