package br.com.rcontarini.movies.data.network.model

enum class Kind {
    NETWORK,
    HTTP,
    UNEXPECTED
}