package br.com.rcontarini.movies.di.component

import br.com.rcontarini.movies.di.module.HomePresenterModule
import br.com.rcontarini.movies.ui.home.HomePresenter
import dagger.Component

@Component(modules = arrayOf(HomePresenterModule::class))
interface HomeModelComponent {

    fun inject(homePresenter : HomePresenter)
}