package br.com.rcontarini.movies.di.component

import br.com.rcontarini.movies.di.module.HomeFragmentModule
import br.com.rcontarini.movies.ui.home.HomeFragment
import dagger.Component

@Component(modules = arrayOf(HomeFragmentModule::class))
interface HomeFragmentComponent {

    fun inject(homeFragment: HomeFragment)

}