package br.com.rcontarini.movies.data.network.datasource

import br.com.rcontarini.movies.data.network.ServiceGenerator
import br.com.rcontarini.movies.data.network.interceptors.UnauthorisedInterceptor
import br.com.rcontarini.movies.data.network.service.MoviesService
import br.com.rcontarini.movies.utils.NetworkConstants


object GenreRemoteDataSource {

    private var mService = ServiceGenerator.createService(
        interceptors = listOf(UnauthorisedInterceptor()),
        serviceClass = MoviesService::class.java,
        url = NetworkConstants.GENRE_API
    )

    fun getGenre() = mService.getGenre(api_key = NetworkConstants.API_KEY, languege = NetworkConstants.LANGUAGE)
}