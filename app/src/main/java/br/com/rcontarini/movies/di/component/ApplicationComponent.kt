package br.com.rcontarini.movies.di.component

import br.com.rcontarini.movies.ui.base.BaseApp
import br.com.rcontarini.movies.di.module.ApplicationModule
import dagger.Component


@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun inject(application: BaseApp)

}