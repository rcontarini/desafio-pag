package br.com.rcontarini.movies.di.module

import br.com.rcontarini.movies.ui.moviesDetail.MovieDetailModel
import dagger.Module
import dagger.Provides

@Module
class MoviePresenterModule {

    @Provides
    fun provideMovieDetailModel() : MovieDetailModel{
        return MovieDetailModel()
    }
}