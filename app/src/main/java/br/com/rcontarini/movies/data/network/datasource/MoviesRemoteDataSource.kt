package br.com.rcontarini.movies.data.network.datasource

import br.com.rcontarini.movies.data.network.ServiceGenerator
import br.com.rcontarini.movies.data.network.interceptors.UnauthorisedInterceptor
import br.com.rcontarini.movies.data.network.service.MoviesService
import br.com.rcontarini.movies.utils.NetworkConstants


object MoviesRemoteDataSource {

    private var mService = ServiceGenerator.createService(
        interceptors = listOf(UnauthorisedInterceptor()),
        serviceClass = MoviesService::class.java,
        url = NetworkConstants.MOVIES_API
    )

    fun getMovies(page : Int) = mService.getMovies(api_key = NetworkConstants.API_KEY, language = NetworkConstants.LANGUAGE, page =  page)

}