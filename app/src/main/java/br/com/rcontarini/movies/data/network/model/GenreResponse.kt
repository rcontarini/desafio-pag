package br.com.rcontarini.movies.data.network.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "genre")
class GenreResponse : Serializable {

    @PrimaryKey
    var id : Int? = null
    var name : String? = null

}

