package br.com.rcontarini.movies.di.module

import br.com.rcontarini.movies.ui.favorites.FavoritePresenter
import dagger.Module
import dagger.Provides

@Module
class FavoritePresenterModule {

    @Provides
    fun provideFavoritePresenter() : FavoritePresenter {
        return FavoritePresenter()
    }
}