package br.com.rcontarini.movies.ui.moviesDetail

import br.com.rcontarini.movies.data.network.model.MoviesResponse
import br.com.rcontarini.movies.ui.base.BaseContract

interface MovieDetailContract {
    interface View : BaseContract.View {
        fun displayError(msg: String?)
        fun showMoviesDetail(item : MoviesResponse)
        fun favoriteAdd(isFavorite : Boolean)
        fun displaySuccess()
        fun displayRemoveSuccess()
        fun questionRemove()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun onFavoriteClicked()
        fun loadMovieDetail(item : MoviesResponse)
        fun success()
        fun error(msg : String)
        fun removeFavorite()
        fun successRemove()
    }

    interface Model : BaseContract.Model<Presenter>{
        fun onFavoriteSave(item : MoviesResponse)
        fun onRemoveFavorite(item : MoviesResponse)
    }

}