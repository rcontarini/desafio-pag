package br.com.rcontarini.movies.data.network.model

import java.io.Serializable

class BaseMoviesResponse(val results : List<MoviesResponse>) : Serializable