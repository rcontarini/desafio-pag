package br.com.rcontarini.movies.di.component

import br.com.rcontarini.movies.di.module.MoviePresenterModule
import br.com.rcontarini.movies.ui.moviesDetail.MovieDetailPresenter
import dagger.Component

@Component(modules = arrayOf(MoviePresenterModule::class))
interface MovieDetailModelComponent {

    fun inject(movieDetailPresenter: MovieDetailPresenter)
}