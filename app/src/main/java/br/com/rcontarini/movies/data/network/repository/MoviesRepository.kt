package br.com.rcontarini.movies.data.network.repository

import br.com.rcontarini.movies.data.db.MovieDao
import br.com.rcontarini.movies.data.network.datasource.MoviesRemoteDataSource
import br.com.rcontarini.movies.data.network.model.BaseGenreResponse
import br.com.rcontarini.movies.data.network.model.BaseMoviesResponse
import br.com.rcontarini.movies.data.network.model.MoviesResponse
import br.com.rcontarini.movies.ui.base.BaseApp
import io.reactivex.Single
import io.reactivex.functions.Function3


object MoviesRepository {

    var mMovieDao : MovieDao? = null

    init {
        mMovieDao = BaseApp.database?.movieDao()
    }

    fun getMovies(page: Int) : Single<BaseMoviesResponse> {
        return Single.zip(
            getGenre(),
            getMovie(page),
            getFavorite(),
            Function3<BaseGenreResponse, BaseMoviesResponse, List<MoviesResponse>, BaseMoviesResponse>
            { t1, t2, t3 -> createMovies(t1, t2, t3) })
    }

    private fun createMovies(genre: BaseGenreResponse, movie: BaseMoviesResponse, favorite: List<MoviesResponse>) : BaseMoviesResponse {
        movie.results.forEach { movies -> movies.genre_ids.forEach { genre_id ->
                genre.genres.forEach {
                    if(it.id == genre_id){
                        movies.genre_name.add(it.name!!)
                    }
                }
            }
        }
        if(favorite.isNotEmpty()){
            favorite.forEach { favorite ->
                movie.results.find { favorite.id == it.id  }?.apply { isFavorite = true }
            }
        }
        return movie
    }

    fun getGenre() : Single<BaseGenreResponse> {
      return  GenreRepository.getGenre()
    }

    fun getMovie(page: Int) : Single<BaseMoviesResponse> {
       return MoviesRemoteDataSource.getMovies(page)
    }

    fun getFavorite() : Single<List<MoviesResponse>> {
        return mMovieDao?.getAllMovies()!!
    }
}