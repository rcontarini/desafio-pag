package br.com.rcontarini.movies.ui.favorites

import br.com.rcontarini.movies.data.network.model.MoviesResponse
import br.com.rcontarini.movies.ui.base.BaseContract

interface FavoriteContract {

    interface View : BaseContract.View {
        fun displayError(msg: String?)
        fun displayMovies(movies : List<MoviesResponse>)
        fun loadMoviesDetail(item : MoviesResponse)
        fun showLoading(loading : Boolean)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun getMovies()
        fun onClickItem(item : MoviesResponse)
        fun setMovies(movies : List<MoviesResponse>)
        fun setError(error : Throwable)
    }

    interface Model : BaseContract.Model<Presenter>{
        fun loadMovies()
    }
}