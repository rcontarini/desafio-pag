package br.com.rcontarini.movies.data.network.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import br.com.rcontarini.movies.utils.AppConstants.OVERVIEW_UNAVAILABLE
import br.com.rcontarini.movies.utils.NetworkConstants
import br.com.rcontarini.movies.utils.extensions.convertToDate
import br.com.rcontarini.movies.utils.extensions.formatToViewDateDefaults
import java.io.Serializable

@Entity(tableName = "movie")
class MoviesResponse : Serializable {

    @PrimaryKey
    var id : Int? = null
    var popularity : Double? = null
    var vote_count : Int? = null
    var video : Boolean = false
    var poster_path : String = ""
    var adult : Boolean = false
    var backdrop_path : String = ""
    var original_language : String = ""
    var original_title : String = ""

    @Ignore
    var genre_ids : List<Int> = arrayListOf()

    @Ignore
    var genre_name : ArrayList<String> = arrayListOf()

    var title : String = ""
    var vote_average : Double? = null
    var overview : String = ""
    var release_date : String = ""
    var isFavorite : Boolean = false


    fun getMoviePath() : String {
        return NetworkConstants.BASE_URL_MOVIE + poster_path
    }

    fun getMovieBanner() : String {
        return NetworkConstants.BASE_URL_MOVIE + backdrop_path
    }

    fun getDateFormat() : String {
        return release_date.convertToDate().formatToViewDateDefaults()
    }

    fun getOverviewTrated() : String {
        return if(overview.isBlank()){
            OVERVIEW_UNAVAILABLE
        } else {
            overview
        }
    }

    fun getGenreFormated() : String {
        var genreFormated = ""
        genre_name.forEach {
            genreFormated = "$genreFormated$it "
        }
        return genreFormated
    }
}

