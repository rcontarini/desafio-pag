package br.com.rcontarini.movies.utils

import android.provider.ContactsContract.Directory.PACKAGE_NAME

object AppConstants {

    const val MOVIES_DETAIL = "$PACKAGE_NAME.MOVIESRESPONSE"
    const val OVERVIEW_UNAVAILABLE = "Sinopse não disponível"

}