package br.com.rcontarini.movies.data.network

import io.reactivex.subjects.PublishSubject

object UserUnauthorizedBus {

    val subject = PublishSubject.create<Any>()

    fun setEvent(error: Any) {
        subject.onNext(error)
    }
}