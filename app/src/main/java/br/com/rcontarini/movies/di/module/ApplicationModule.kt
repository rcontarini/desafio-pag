package br.com.rcontarini.movies.di.module

import android.app.Application
import br.com.rcontarini.movies.ui.base.BaseApp
import br.com.rcontarini.movies.di.scope.PerApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class ApplicationModule(private val baseApp: BaseApp) {

    @Provides
    @Singleton
    @PerApplication
    fun provideApplication(): Application {
        return baseApp
    }

}