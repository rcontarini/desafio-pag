package br.com.rcontarini.movies.ui.favorites

import br.com.rcontarini.movies.data.db.MovieDao
import br.com.rcontarini.movies.data.network.model.MoviesResponse
import br.com.rcontarini.movies.ui.base.BaseApp
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class FavoriteModel : FavoriteContract.Model {

    private lateinit var presenter: FavoriteContract.Presenter
    private  var movieDao : MovieDao? = null

    init {
        movieDao = BaseApp.database?.movieDao()
    }

    override fun loadMovies() {
        movieDao?.getAllMovies()?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe( object : DisposableSingleObserver<List<MoviesResponse>>() {
                override fun onSuccess(t: List<MoviesResponse>) {
                    presenter.setMovies(t)
                }
                override fun onError(e: Throwable) {
                    presenter.setError(e)
                }
            })
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
    }

    override fun attach(presenter: FavoriteContract.Presenter) {
        this.presenter = presenter
    }


}