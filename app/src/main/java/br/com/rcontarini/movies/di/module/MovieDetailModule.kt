package br.com.rcontarini.movies.di.module

import br.com.rcontarini.movies.ui.moviesDetail.MovieDetailPresenter
import dagger.Module
import dagger.Provides

@Module
class MovieDetailModule {

    @Provides
    fun provideMoviePresenter() : MovieDetailPresenter {
        return MovieDetailPresenter()
    }
}